扎爾穿上了咔嚓作響的鎧甲


我也出手幫了他一下

武器是長矛


金屬製的傢伙，相當沉重，但扎爾似乎毫不介意，輕而易舉就拿了起來

「你有練過麼？」

「沒有。但在這個世界裡，總能做到點什麼……不過姿勢之類的就沒辦法了」

實際上，扎爾擺出的架勢，腰部根本沒有注入力量

這樣子是不行的吧，我想到


要問這是在做什麼，是因為我們決定，把昨天看到的那東西打倒

扎爾說，把它打倒的話，莉亞應該就能返回現實世界了

問了下理由，他卻只是支支吾吾，不願意詳細說明

雖然也有他在說謊的可能性，但我在這個男人的眼神中，看到了作為父親的那種既嚴厲、又溫柔的感情


如果這也無法信任的話，我可能之後就再也無法相信別人了吧

那是一個下定決定的父親的眼神

「……抱歉吶，雷特」

「哎？」

「沒辦法一一解釋清楚，不過我也會和你並肩戰鬥的」

「沒關係啦……而且，能和本應已經死去的你說上話，我也很開心。事故發生之前，還沒來得及和你打聲招呼呢」

「哈哈，這就好……我也想好好向你道聲謝呢，感謝你幫助了莉亞她們」

「是說在市場上時候麼？」

「沒錯。因為職業的關係，我沒法一直守在莉亞身邊。如果當時沒有你在……」

「那就不必在意了，因為是家常便飯呢」

「你真是個好人吶，莉亞也是這樣說的」

「……這樣評價我的麼，雖然是個七歲小孩」

「就是這樣評價的呢，那個七歲小孩」

我們這聊著，笑了起來

♢♦︎♢♦︎♢

「……聽好了，莉亞。今天晚上，你絕對不能離開這裡」

「……知道了」

夜幕降臨


扎爾對莉亞這樣矚咐之後，關上了小屋的房門

又從外面插上門閂，防止門被打開


然後，向著森林的深處邁出了腳步

我也跟在一旁

「做好準備，雷特。要來了」

扎爾這樣說的同時，有什麼東西揚起塵土，從森林深處衝了出來

體型龐大的怪物

是那傢伙沒錯了

雖然知道對方是個龐然大物，但真的看在眼裡時，還是止不住顫抖

比我以往見過的任何魔物都要大

宛如城堡一樣的巨蛇

「……要打倒這傢伙麼？」

「沒錯，加油吧！我們能做到的」

說出這話的扎爾顯得非常可靠

雖然實際上，他只是個沒有戰鬥經驗的普通人，但在這一戰中卻展現出了仿彿三頭六臂般的力量

一躍而起，揮動長槍，掃斷樹木

宛如英雄一般

這種事情，我……

似乎是察覺到了我的想法，扎爾說到

「這裡是夢境，相信的事情就能夠實現，請鼓起勇氣來！」

被提醒了呢，真是危險


差點就被吞沒了

來這裡之前，羅蕾露也曾說過


待在夢境世界裡一久，就會變得難以區分夢境和現實

所以，必須相信自己

只要能夠相信自己，就能夠一往無前

沒錯，相信自己

夢想，未來，可能性


這是我一直以來都在做的事情

不會輸給任何人

突然間，我感覺自己的身體靈巧了起來


手上握著慣用的單手劍，也仿彿變成了大聖堂供奉著的，傳說中的神具一般

全身充斥著魔力和氣力，巨蛇的動作也能看得一二楚了

……輕而易舉

這樣想著，我猛踏地面

僅僅一躍，便來到了蛇眼前


我的夢想

英雄般的力量


想要成為秘銀級，沒有這種程度是不行

沒錯，我什麼都能做到


舉起劍來，一擊斬斷它的脖頸

一定能夠做到！

將信念注入劍中，使出全身力氣，猛地揮落下去


我一擊便將巨蛇的頭斬飛了出去

「……雷特……！不愧是你……！」

著陸的同時，扎爾對我露出了笑容

「這是我的夢想啊，總有一天，要達到這種高度」

「一定會成功的……我會為你加油……哦……」

話還沒說完，扎爾的脖子上出現了一道線


筆直的線

宛如被毫無遲滯、完美的一擊斬中的直線……不會吧

「扎爾！喂！」

咕嚓，扎爾摔倒在地

同時，他的頭也從脖子上掉了下來

但不愧是夢境世界啊

儘管如此，扎爾還是發出了聲音

「……雷特，對不起吶，看來我就到此為止了呢。你，帶著莉亞，到外面……跑到遇見我的那片森林，那樣的話……」

「這是怎麼回事……」

「那條蛇，就是我。是莉亞創造出來的，咒縛。巨大的，巨大……然後，夢魔佔據了那個存在，使其進一步肥大化……我自己也，和那差不多，所以……」

那就是把莉亞束縛在夢境世界裡的東西了吧


同時也是夢魔佔據此處的楔子

也就是說，打倒了那條巨蛇，夢魔應該已經隨之消火，或者離開了


但與此同時，巨蛇與扎爾也是相通的存在

所以現在，他才會……

「既然如此，為何要我去打倒巨蛇……」

「我是那孩子的父親。如果能在這裡一直生活下去的話，會很幸福吧……但那可不行。終有一天，我們還會見面的吧。在那之前……我希望她能好好活在現世中……你來了，正是個好機會。因為憑那個孩子一人，很難走得到出口吧……雷特……拜託了哦……請代我向修米問好」

說完這些話，扎爾的身體一下子變成了灰色，然後像沙子一樣崩塌了

目睹了他的最後，我站起身來

來到小屋門口，拔出門閂，呼喚莉亞

「莉亞！」

「……雷特哥哥？咦，爸爸呢？」

「……邊走邊解釋，但是現在……」

--轟隆！！


巨聲響起，世界晃動起來

向遠處望去，可以看到，大地正在塌陷


這個夢境世界，正在迎來終結吧

「來吧，我們走！」

我握緊莉亞的手，但她顯得有些猶豫不決

這裡是莉亞的夢境

即使我不說，她也已經察覺到了一些東西吧


但是……

「莉亞！扎爾有話轉達給你！你要好好活在現世！醒過來吧！」

喊出這些話後，我感覺她握住我手的力道加強了一些

我們就這樣奔跑起來

「要去哪裡？」

「說是有出口……在最初的森林……這邊！」

地點我還記得

之所以會這樣，是因為第一次見面的時候，扎爾一直固執地提醒我要注意四周


那個時候，他就已經打算好了吧

真是個稱職的父親


來到目標的場所，那裡有一個發光的，門一樣的東西

「……就是這裡，準備好了嗎？」

「……雷特哥哥，但是，我……」

還在猶豫麼，但這也是理所當然的

因為失去父親而留下傷痕，又被夢魔趁虛而入


而且他還在那裡……

但是……

「……這個給你」

我把一樣東西交給莉亞

那是一個小小的項鏈

在和巨蛇戰鬥之前，扎爾把它托付給了我，要我轉交給莉亞

說是打算自己給的，但是以防萬一，我也只好先收下了


這大概也是從一開始就打算好的吧

「這是……爸爸的……」

莉亞看著項鏈留下了淚水

雖然不知有什麼樣的故事

但這件東西，對她來說，一定有什麼特別的意義吧

「……雷特哥哥，走吧！」

看來已經下定決心了

莉亞握緊了我的手，向門走去

「啊！」

我也點點頭，和她一起走了起來

大地在我們背後不斷陷落

差一點就被追上了


在那之前的瞬間，我們跳進了門裡

♢♦︎♢♦︎♢

「……雷……特……雷特！」

聽到這樣的聲音在耳邊響起，我一下子坐了起來


眼前是羅蕾露熟悉的面龐

往四下張望，修米也在一邊

再往旁邊看去，她應該就躺在那裡……哎，沒有


我歪了歪頭

「莉亞的話，比你先醒過來了。現在正在接受治療師殿下的診察。因為好幾天沒吃東西了，需要確認一下身體狀況」

「啊……這樣啊，已經醒了麼。太好了」

「只是沒想到你會醒來的這麼遲，消耗了那麼多精神力嗎？發生什麼事了？」

我向羅蕾露說明了夢境中發生的事情

修米也在一旁聽我講述

「原來如此……那樣的話，蘇醒得這麼遲也是可以理解的。干涉別人的夢境是相當不容易的，要做到那些事情，自然也會有相應的消耗」

「是麼？」

「沒錯，說在夢境中無人能敵，也僅僅是對自己的夢境而言。在別人的夢裡……如果沒有相當強大的意志力，是沒辦法做到什麼的，即便那是小孩子的夢也一樣」

……喂

這不就是說，我剛剛闖入了一個非常危險的地方麼


雖然被羅蕾露鼓勵，我自己也覺得能夠做到，但一個不小心，就可能束手無策死掉了吧

好在最後我做到了吶……而且被他人信賴，感覺也還不錯


聽了我的話，修米似乎有什麼事情要說

聽到一半就開始止不住流淚，現在正用羅蕾露遞過去的手帕擦著鼻涕……她也有這樣的一面啊


不，因為非常混亂，才會表現出來的吧

「……那個人，是扎爾救了莉亞吧……」

「哎，雖說束縛住莉亞的也是他自己，但是最終將其解放的也是扎爾。他還讓我代他問候你和莉亞」

「很有那個人的風格呢……不過，太好了。這樣一來，我就能繼續和莉亞一起前進了呢。果然，那個人突然不在了……我也不知道該如何是好啊。但是，這樣下去會被人嘲笑的，我一定要連他的份一起，保護莉亞才行呢」

「……是啊，這樣是最好的」

看來，這次的事件，也不全是壞事呢

我突然想起來一件事，問到

「說起來，在夢境世界裡，我最後把項鏈遞給了莉亞。但果然，那是沒辦法帶出來的吧？」

被我問到的羅蕾露歪過頭

「……嗯？究竟如何呢。莉亞醒來後，直接去檢查身體狀況了，所以不太清楚……但在夢境世界裡獲得的東西，應該是無法帶出來的」

「也是啊……嘛，這樣一來，委託就算是達成了」

「是啊……修米小姐，能認同我們達成委託麼？」

我一邊遞過委託書，一邊這樣問到，修米點點頭

「當然」

然後簽下了名字

♢♦︎♢♦︎♢

「……雷特哥哥！」

我正在市場上走著，被人打了招呼


回頭一看，是莉亞

修米也站在她身後，笑眯眯地看向這邊


那次事件之後，她似乎非常信任我和羅蕾露

有時還會送些禮物過來，真叫人難為情

「莉亞，今天也很精神啊……」

「嗯，哥哥今天也要去抓史萊姆嗎？」

「是啊……要不要乾脆開個化妝品店試試呢，和羅蕾露一起做的話，感覺可以開一間商店呢」

「啊哈哈，開玩笑的吧？」

「那是當然啦……嗯？那是……」

我們這樣閑聊著，突然，我看到一件東西從莉亞身上垂下來

「啊，這個？是在那個世界得到的東西哦」

「唉？不會吧，羅蕾露說，應該是不能帶出來的……」

我正有些困惑

「莉亞！走吧！」

修米叫了莉亞

莉亞這樣答道

「再見了，雷特哥哥！」

說著跑了過去

被留在原地的我，思考著項鏈的事情


夢境世界的東西，不能帶到外界來

理應如此才對

這就是規則


但是，剛剛那確實是……

如果有那種東西存在的話，恐怕所有人都會想要的吧，羅蕾露說過……

「……嘛，算了，保持沉默就好了吧」

乍一看，只覺得是普通的東西而已


除非用特殊的方法去調查，否則是不會發現什麼端倪的

就算說這是從夢境中帶出來的物品，又有誰會相信呢

最重要的是，不能讓別人奪走莉亞父親的遺物

所以，這件事情也對羅蕾露保密好了

這樣想著，我把這件事拋在了腦後


今天也要進入《水月迷宮》啊


一成不變的日子再次開始……