以礦山為目標的杰羅斯等人抵達了阿哈恩村。這時太陽早已開始下沉，他們便在價格不貴的旅館住了一晚。接著到了隔天，在吃完早餐後，因為伊莉絲無心的一句話而開啟了話題。

「叔叔，法芙蘭大深綠地帶是什麼樣子啊？我從傭兵公會給的情報中只知道那裡很恐怖，但到底是怎樣的地方呢？」她這麼問。

以杰羅斯的立場而言，他其實不是很想說。然而他還是希望避免認識的少女半是好奇地便跑進去，就此成為不歸人。在判斷要是事先告訴她那裡是危險之處，就能避免她去做傻事了之後，杰羅斯一邊嘆氣、一邊將雙手交握靠在桌上，開口述說。

述說那個大深綠地帶是多麼危險的地方。

述說那個他一點都不想回憶起的恐怖體驗。

◇　◇　◇　◇

轉生到異世界的第五天。杰羅斯喘著氣，不斷地走在深夜的森林中。

周圍全是些想要吃掉自己的魔物，在它們的眼裡看來自己只是一塊餌食。

隱藏氣息、摒住呼吸，得對周遭保持警戒才行的日子持續著。

「咕……到底要持續到什麼時候！要怎樣才能離開這座森林……」

平常的說話方式變回了以前年輕時的樣子。表情雖然十分憔悴，仍露出有如野獸般凶狠的目光。現在的他心中沒有任何餘裕。

周圍滿是只要他一有空隙就會襲來的魔物，吃飯及睡覺時也得持續保持警戒，精神上的疲勞不斷地在累積。片刻都無法休息的戰鬥，不斷葬送敵人的修羅之宴。

雖然魔物本身是不強，但每打倒一批，血腥味又會引來別的魔物。大型魔物是單獨、小型魔物則是集體前僕後繼的襲來，所以精神與體力的消耗都相當劇烈。

他在戰鬥告一段落的期間調整呼吸，然而仍未放鬆警戒，注意著周圍。因為只要些許的大意就會喪命。

「！」

忽然從背後感覺到的氣息，使得杰羅斯立刻飛也似地逃離現場。

──斬！

瞬間，地面上刻了一道細長的痕跡。雖然這是來自某物的攻擊，卻不見其身影。

他動員身上所有感覺來探索周圍的氣息與動靜。

『有什麼在……很明顯地在看著我……在哪裡？』

看不到敵人的身影，但是肯定有個顯然是以自己為目標的捕食者在這裡。

野獸雖然不會挑戰強者，但對手很衰弱的話就另當別論了。弱肉強食是原始的自然法則。不打倒對手、吃下對方就無法生存才是大自然最初的樣貌。

雖然不知道對手是怎樣的魔物，至少可以確定對方有可以隱藏身影的能力。

「問題是屬於哪種類型的……是光學迷彩，還是精神干擾型的呢……」

光學迷彩是以魔力操縱大氣中的水分，利用光的折射來使周遭看不見自己身影的能力。精神干擾則是放出特殊的波長，使對手的五感出現錯覺，讓獵物無法正確察覺到自己的能力。

既然可以感覺到攻擊的氣息，就不是後者。他判斷有很高的可能性是光學迷彩。

再度感覺到氣息，杰羅斯憑恃著本能高高躍起，將鋼絲鈎上樹齡不知有幾百年的大樹的枝幹上。他剛剛站的地方又畫上了幾道痕跡。

很明顯是斬擊型的攻擊。不過這次他稍微看到了對方的樣子。

正確來說是有如空間的扭曲那樣的東西，但可以看出對方是大型魔物。其他魔物之所以沒有襲來，也是因為害怕這只魔物吧。

「貫穿吧，『狩神之矢』。」

漂浮在周遭的塵埃瞬間凝聚壓縮，化為足以貫穿鋼鐵的箭矢射向了空間扭曲之處。

──嘰呀啊啊啊啊啊啊啊啊啊啊啊！

魔物或許是感到痛楚了吧。箭矢恐怕是貫穿了它的背部，然而由於看不見身影，也無法確認給予的損傷。接著空間搖晃了一下，解開光學迷彩後，現身的是巨大的螳螂。

那身影強悍得簡直異常，黑色的外殻與誇張、有如長鐮刀一般的手臂，支撐那巨大身軀的腳又粗又長。銳利的爪子耙著地面，複眼閃著深紅的光輝。

「死亡螳螂……」

在遊戲中算是相對輕鬆可以打倒的魔物，可是眼前的存在卻散發出遠比那更強的氣息。儘管一方面也是因為等級不同，但是那個樣貌和他所知的死亡螳螂有巨大的差異。

長滿了無數尖銳的突起物，有著保護自己不受外敵侵擾的形貌。

「是進化種或是亞種吧。真麻煩……不過昆虫不是沒有痛覺嗎？」

雖然心中浮現了如此基本的疑問，但這裡是異世界，有著他所不知道的常識，所以他的知識也並非絕對。不管怎樣他都得打倒這個魔物，然而裹著昆虫型外殻裝甲的魔物是很麻煩的對手。魔物會讓魔力流入堅硬的甲殻中來提升防御力，所以這邊也得將魔力纏繞在武器上，打消對手的防御力來戰鬥。

儘管是可以輕鬆打倒的對手，可是在不知道這個森林到底有多廣闊的情況下，他想盡可能地避免去消耗魔力。就算保有的魔力再多，也還是會迎來極限的。

在嚴苛的環境下，魔力──魔法是維繫生命的生命線。不能浪費。

杰羅斯衝向死亡螳螂的懷中。捕捉到杰羅斯的動作，死亡螳螂以驚人的速度揮下了鐮刀手臂。然而這正是杰羅斯的目的。

正因為身軀巨大，動作也很大，雖然很快，但也不是無法看穿的速度。

「就是這裡！」

短劍沒入了連接著鐮刀手臂的關節處。這是奪去敵人戰鬥能力的常見手段。

該瞄準的是巨大的鐮刀。杰羅斯的斬擊讓死亡螳螂的鐮刀飛到空中後，刺入了地面。

在確認到這點之前，杰羅斯便快速奔走，鎖定最脆弱的關節處，以雙手上的劍在瞬間反覆使出斬擊，在同一處同時攻擊兩次，將長長的腳切成好幾斷。

他立刻重複相同的行動，把對手的四肢全部砍飛。死亡螳螂倒在地面上之後，杰羅斯便砍下了它的頭部給予致命一擊。真的是瞬間便打倒了對方。

他迅速地將已經變為屍體的死亡螳螂給解體，急忙收入道具欄中之後，杰羅斯又再度為了離開森林而跑了起來。要是不趕快離開這裡的話，很有可能會再被其他魔物襲擊。

魔物對血腥味極為敏感。然而這裡是聚集了眾多強力魔物的魔之森。

這裡是就算打倒了魔物，瞬時又會有其他魔物襲來的地獄。

──嗡嗡嗡嗡嗡嗡嗡嗡嗡嗡……

殘留在耳中的重低音振翅聲。杰羅斯轉向振翅聲傳來的方向，看到了那傢伙。

黑色外殻光滑油亮的最強生物。在地球上從太古時代開始便未曾改變過其姿態存活至今，充滿生命力的昆虫。而且非常巨大。

「強、『強大巨蟑』……」

比白猿更不想碰到的傢伙正以高速飛來。

強大巨蟑在地面上造成巨響後著地。那是遠比死亡螳螂更加巨大的最強昆虫型魔物。

長長的觸角一邊動著，一邊以複眼環視周遭，尋找餌食。發現到杰羅斯后……

──颯颯颯颯颯颯颯颯颯颯颯颯颯颯颯颯颯！

它以超高速跑了過來。老實說感覺超噁心的。而且還是以一路揚起沙塵、撞倒樹木的氣勢逼近。從各方面來說杰羅斯的臉上都失去了血色。

「No──────！Oh──My God！Help，Help Me──！」

因生理上的嫌惡感與內心的動搖讓他放棄當日本人了。大賢者討厭男同志，也討厭小強。

不如說要是有人喜歡的話，在各種意義上都想勸他往生物學相關的方面發展。

「為什麼……為什麼只有小強的樣子，跟原本的世界一模一樣啊──────！」

沒錯，就連螳螂都長了一些凶惡的突起物，使得外型有所改變，但只有小強的外表和原本的世界完全沒有差別。而且這從太古以來從未改變外型存活至今的生物，在異世界變得有如恐龍一般巨大。要是跟在地球上一樣小，就能立刻用拖鞋殺死它了，然而面對全長約十公尺等級的龐然巨物連這點辦不到。而且還很不必要的強而有力。

最糟的是鑑定技能還不知為何沒在運作，連想查看對方的資料都辦不到。簡直讓人覺得這根本是有人在惡整他的狀況接連發生。

經歷了這個體驗之後，他就莫名的討厭起害自己來到這個世界的女神。

逃亡的杰羅斯與追著他的強大巨蟑。激烈且令人討厭的恐怖鬼抓人就這樣開始了，而且就這樣一直持續到了早晨。大叔悲痛的叫聲響徹了整個廣大的森林。

諷刺的是，多虧了這場鬼抓人，他才能來到接近道路的地方。然而杰羅斯並不知道這件事。被討厭的小強其實做了件好事。

◇　◇　◇　◇

「────發生過這樣的事呢。哎呀，你們怎麼了？」

聽完杰羅斯的冒險故事，三位女性隊伍成員──伊莉絲、雷娜、嘉內全都趴在桌子上。

想著『小強……好可怕……』，或是『巨大的小強魔物……我一點都不想碰到那種東西……』，還是沉默地全身發抖，三人各有不同的反應。

早餐時間，她們聽了曾踏入大深綠地帶的熟練者所帶來的經驗談後，在想像巨大的小強時精神集體崩壞了。

「叔叔……還真是經歷了一場不得了的大冒險呢……」

「杰羅斯先生，真虧你能活著回來呢……法芙蘭大深綠地帶的魔物太凶惡了，明明沒有人會接近那裡的……」

「雖然能夠打倒那魔之領域的魔物也很驚人……可是巨大的小強，嗚哇！」

強大巨蟑目前確認到的最大尺寸是五公尺等級。

就算那擁有足以拔山倒樹的巨大體型的個體尚未被確認，光是聽這些敘述就可以想見那魔物凶惡的超乎想像。而且除了外觀之外，它還會高速移動，外殻也非常的硬。

想必是因應其大小，裝甲也變厚了吧。要是不使用攻城兵器，恐怕是難以傷它分毫。

被那種強得誇張的魔物襲擊，就連要逃跑都很困難。

然而她們卻是因為別的意義而感到恐懼。杰羅斯也抱持著一樣的心情。

「比起那個，趕快吃完早餐吧？還要去礦山呢。」

「就算你這麼說～……」

「巨大的小強……我的食欲……」

「……………（抖抖抖抖）」

一旦想像過那景象，她們的思考便無法停止。

那強悍的身影不斷地閃現在腦中，讓她們失去了食欲。

「不過要是這樣就會失去食欲，根本沒辦法當傭兵吧？有時也要跟人類互相廝殺不是嗎？山賊或盜賊，偶爾還要跟其他傭兵戰鬥。」

「叔叔，你可以不要說那種討厭的話嗎？我們是專門對付魔物的喔。」

「對啊，沒辦法以人類作為對手啦……就算是壞人也一樣。」

「所以你們兩個才會被抓起來吧？連對要加害自己的對手都無法下手，要怎樣才能活下來啊。傭兵是得自己對自己負責的工作吧？要是因為這樣死了那可一點都不好笑喔。」

伊莉絲和雷娜似乎沒有殺過人，所以才會因為猶豫而被盜賊們抓了起來。

想在性命被看得不是那麼重要的這個世界存活下來，這是過於天真的想法。

「唉，雖然不是可以笑著殺人的人這點是很令人有好感啦，但做好殺人的覺悟會比較好喔。」

「我雖然有殺過人，但那實在不是什麼愉快的經驗。」

「要是殺人會感到愉快的話，那個人就是有什麼精神上的疾病喔。做隔離處置比較好。」

嘉內似乎有殺過盜賊的經驗，但不覺得殺人是件好事。

既然身為傭兵，有時也會與犯罪者對峙，為了保身的殺人行為也是被允許的。而若是被殺了也是自己的責任，所以杰羅斯認為這是種很討厭的差事。

「要是吃不下的話，請店家打包當作便當如何？」

「啊，還有這招！」

「便當？那是什麼？」

「沒聽過的詞彙呢，雖然伊莉絲好像知道是什麼。」

這個世界裡沒有所謂的便當。工作的人大多都住在城裡，也有很多將自家當作工作室的工匠。就算不自己做飯，只要出門就有一堆餐廳。

而且也有攤販，所以完全沒必要帶便當。

「是以面包夾了稍微調味過的燻肉及蔬菜，再用紙包起來帶著走的東西。除了特別炎熱的時期外，應該也不用擔心食物會壞掉。」

「原來如此，反正行李是伊莉絲幫忙帶……」

「真是個好點子！伊莉絲，可以拜託你嗎？」

「沒問題～♪那我去跟旅館的叔叔說。」

伊莉絲踏著輕快的腳步前往廚房。阿哈恩村的旅館店長好像也兼任廚師，以跟某處的船上旅館類似的方式在營業。

「是說死亡螳螂的素材是怎樣的東西啊？可以拿來做裝備嗎？」

「很適合用來做武器或防具喔。重量輕又很堅固，但是不耐火系魔法。」

「畢竟是昆虫嘛，火是弱點吧。」

「甲殻中的肉可是絕品喔。吃起來的味道就像螃蟹或蝦子呢。」

這句話讓空氣靜止了。雷娜和嘉內的表情僵住不動。

「你、你吃了嗎？死亡螳螂……」

「那可是魔物喔！你認真的嗎？」

「說是這麼說，但依據種類不同，獸人一類的肉一般也會拿來食用吧。有什麼奇怪的嗎？畢竟我那時候可是過著要優先確保食物來源的野外求生生活，要是沒有毒就該吃吧？」

「雖然……是這樣沒錯，但那是巨大的昆虫喔？」

「一般來說不會想到要吃吧……至少我沒辦法！」

「要是抱持著那種心態，真的發生什麼事的時候可是會餓死的喔。生存是一場戰鬥啊……沒錯，就算說是戰爭也不為過……」

『他……他的眼神好可怕……』

『就算有過恐怖的經驗，這也不太妙吧？我決定以後絕對不要去那座森林……』

杰羅斯在原本的世界就會吃佃煮的蝗虫，所以對於食用昆虫這件事沒有太多的抵抗。只有小強在生理上完全無法接受。這點先不提，明明在吃魔物這點上意義是相同的，她們兩人卻陷入了比較昆虫與動物之間差異的思考迷宮中。

「他說可以幫我們做便當喔～……怎麼了？」

「是我們錯了嗎……？可是那是昆虫耶……」

「虫不行……沒辦法吃虫。可是獸人也是魔物，差別到底在哪裡？」

回來的伊莉絲不解地歪著頭。

在她們身旁的杰羅斯點燃菸草，享受飯後的一根菸。大叔吹出的煙圈靜靜地從煩惱中的兩人頭上飄過。

◇　◇　◇　◇

在阿哈恩村的背後有座擁有三座山峰的高山聳立著。

礦山就在其中一座山峰中，有段時間曾因前來尋求豐富金礦的礦工們而顯得熱鬧非凡。

但以某個時間點為分界線，那裡出現了大量的魔物，礦工們被迫離開了職場。接下來過了約兩百年，現在會來拜訪這座礦山的人，變成為了提升等級或採礦來強化裝備的傭兵們，而他們付出的住宿費和付給攤販的餐費就成了村裡的收入。

儘管如此這個村莊的生活仍稱不上富足，有時也會發生因為無法無天的傭兵而被迫付出不少額外的花費。阿哈恩村在某種意義上來說已是風中殘燭。

「人還滿多的耶？大家都是傭兵嗎？」

「雖然打倒魔物就會升級，但我們畢竟是賭命在工作的，也想順便充實裝備啊。在這個礦山既可以得到金屬，礦工們若是沒有人護衛也沒辦法進去。」

「唔嗯……也有看起來性格十分惡劣的人在耶，你看那邊……」

在他們視線前方的，是看來還是個年輕少年、初出茅廬的傭兵，以及一群圍著他的中年傭兵們。儘管看起來就是一副雜魚樣，但至少還是比少年來得強吧。

「有什麼關係，反正你也用不好吧？我來代替你用吧。」

「這是我爸爸的遺物，我不會交給任何人的！」

「比起像你這種乳臭未乾的小子，你老爸也一定比較希望給我這種老練的傭兵用啦！」

「你根本就不了解我爸爸，請不要在那邊擅自胡說！」

「我很清楚，因為我是個老練的傭兵啊。那把劍需要怎樣的手腕才能使用，我一看就知道了～給你這種雜魚太浪費了。」

他很明顯的在找藉口來將少年的劍據為己有。周圍的伙伴們則是以下流的笑聲在嘲笑少年。雖然附近也有其他的傭兵，但沒有任何人去幫助少年。

「還是有呢，這種幹著老套蠢事的笨蛋們……」

「叔叔，你不能做點什麼嗎？」

「為什麼要推給我？在意的話你就自己去幫他啊。」

「杰羅斯先生……一個優秀的大人這樣做真的好嗎？」

「平穩度日是我的座右銘。我一點都不想介入他人的糾紛之中。」

「……真是個差勁透頂的大人。」

冷淡的視線集中在杰羅斯身上。對當事人來說，雖然不想惹上多餘的是非，周遭的人卻很期望他這麼做。就在他正想著這種事的時候，狀況忽然急轉直下。

「就說了叫你把東西給我！」

「啊，還給我！請把它還給我！」

看準空隙，男人將少年的劍從劍鞘中拔出並奪走。

就在這時候，杰羅斯明明不想使用，「鑑定」技能卻擅自發動了。

＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝

【秘銀之劍（劣）】

原本是由知名矮人所鍛造的頂尖之劍，然而劍身已有裂痕。面對弱小魔物的話還可以用上一陣子，但差不多快到使用年限了。已經老朽到幾乎不能作為一把劍來使用的程度。

儘管重新鍛造會比較好，但由於需要大量素材，要有賠本的覺悟。耐久度已經降到只要承受大型魔物的一擊便會碎裂的程度。

＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝

『為什麼在不需要的時候發動了啊。真是令人火大……』

明明沒需要卻擅自發動的技能真是可恨。嘆息與抱怨從口中逸出。

「嘿嘿嘿……得到一把好劍啦。」

「還、還給我……那是我爸爸的……」

「吵死了！對小鬼來說這東西太高級了，我就感激地拿來用啦。」

打飛了少年，粗俗的傭兵一臉爽樣。

「啊～不好意思在你搶了別人的劍正開心的時候打擾你，但那把劍不能用喔？」

「什、什麼啊，你……」

「沒什麼，我只是個路人，無須在意。那把劍已經破破爛爛，要是繼續使用的話你很快就會死了。雖然對我來說是沒差啦。」

粗俗的傭兵和伙伴們面面相覷。

「你、你少騙我了！」

「要是有水的話我就可以證明給你看了，但我沒義務做到那種地步。只不過我有『鑑定』技能在，不小心看到了才給你個忠告罷了。」

「鑑定」技能可以因應等級，看到東西的詳細資料。雖然這對人類也適用，甚至可以得知對手的等級，但觀看他人的資料這件事本身是違法的。

「信不信隨你喔？畢竟要死的也是你，反正是別人的命，我是怎樣都好啦。」

「什麼！……這是真的嗎？」

「這點就請你自己判斷吧。我已經說了，信不信隨你。我話就說到這裡了。」

眼前的魔導士實在太可疑了，說謊的可能性很高。可是，要是眼前的魔導士說的是真的，那他說不定就會因為這把劍而喪命。男人在兩個選擇間搖擺不定時，注意到了杰羅斯腰上的劍。第三個選項出現了。

從身為傭兵的男人眼裡看來，那就是個身材一般、十分邋遢，跟自己年紀相仿的魔導士。感覺完全不像會用劍，更重要的是還是個灰袍。男人露出低俗的笑容提議。

「既然如此，跟你的劍交換如何？」

「不需要。我用自己的就夠了。」

「不不不，這可是秘銀之劍喔？比你的劍高級吧？」

「還真纏人啊，我不需要那種壽命將近的劍。你去找別人吧。」

打算交涉卻被乾脆地拒絕了。而且還是說這是有缺陷的東西而拒絕，看來秘銀之劍無法使用的可能性很高。只是看著眼前的魔導士的劍，因為對方是魔導士，所以還有使用蠻力強奪這個手段在。那名傭兵猥褻地舔了舔嘴。

「你啊，是魔導士吧？根本不需要劍吧，就由我來……！？」

男人的話沒能說到最後。要說為什麼，那是因為他的喉頭正被不知何時出現的劍尖給抵著。從被劍尖刺入些許的喉頭上流下了一條血痕。

「我啊，不是不會用劍喔。不如說我還比較擅長這個呢……所以呢？你想死嗎？畢竟回擊惡劣的傭兵也沒什麼問題，我是不會客氣的喔。怎麼樣？你想打的話我可以奉陪，但請你做好會死的覺悟喔。」

「咿！」

「什麼啊。他是什麼時候……把劍……」

「被騙了……這傢伙不是魔導士！」

杰羅斯的拔劍速度就是快到讓人連他是幾時拔劍的都不知道。同伙的傭兵們了解到眼前的對手具有相當的實力，是自己無法勝過的對象。

現在那名傭兵就等同於被眼前的魔導士給抓著心臟一樣。

「明明實力就不怎麼樣，不該奢求好武器吧。你是小孩子嗎？只會依賴武器的話不就永遠是三流的嗎？我是不知道你是不是想送死啦，但這個時間點上還不知道對手的實力，我想你不管怎樣都不會長命的喔？唉，連三流都稱不上呢。既然如此你要不要死在這裡算了？」

可疑的魔導士瞬間變成了技術高超的怪物。

魔導士以沉靜卻充滿恐嚇氣息的語調說的這番話，讓男人的心被恐懼給束縛。對方雖無殺意，但可能會看心情便殺掉他。

這個事實將男人推落了無法形容的恐怖深淵。

「我啊～老實說很不爽呢。明明是來採礦的，卻得當你們這種垃圾的對手……你知道嗎？」

會對這種初出茅廬的傭兵出手的傢伙，大多都沒什麼實力。

判斷他們不會反抗比自己強的對手，他才稍微試著威脅看看的。看來他這麼做是對的。畢竟當中也有不會考慮雙方實力差距就出刀的傢伙，那種狀況是最糟糕的。

所以為了預防發生什麼萬一時不會使周遭的人受害，他絲毫沒有大意。

「啊……啊啊……抱歉。是我太得意忘形了……」

「你知道就好。趕快歸還那把劍，從我的面前消失吧？要是你繼續在我們面前閑晃的話，那時候……」

「那、那時候……？」

「我就送你去趟快樂的旅行吧。雖然再也沒有辦法回到這裡來就是了……咯咯咯。」

傭兵們拋下了劍，全都從露出冷徹笑容的魔導士身邊逃開了。

不會推測對手的實力，只會憑著氣勢做事，依靠這種惰性持續當著傭兵的他們，是沒辦法對付比自己強的對手的。

逃跑的速度似乎是一流的，他們以驚人的速度跑走了。

儘管對這老套的發展感到傻眼，杰羅斯點燃菸草，吐出一口煙。

「呼……小混混是不是很多那種人啊～？」

剛剛那種強得嚇人的感覺不知道上哪去了，瞬間變回邋遢的大叔。

少年走到了這樣的大叔身旁。

「那、那個……謝謝你。」

「嗯～～沒什麼，不用特別謝我。我只是硬是從旁介入罷了……」

「不，因為這是父親的遺物……真的很謝謝你！」

「別在意、別在意。不過就是今天『運氣很好』而已。傭兵這工作就是不知道明天會怎樣啊。」

有些角度看來有如少女般的少年給人受過高等教育的印象。出身看起來實在不像會當傭兵的樣子。

身上穿的裝備也是，外觀看起來雖然像是一般市面上販售的東西，卻用了高級的素材。同時，他也察覺到少年的眼中閃著藏有覺悟的強力光芒。

『唉～……看來他隱藏著相當強烈的決心呢。要是在這裡分別後他因為劍而死了，我會睡不好覺的啊～沒辦法，只好多管閑事了。』

不知為何有些在意少年的杰羅斯，從道具欄中取出了一張紙攤在地面上。那是鍊金術的高等技巧「鍊成魔法陣」。

「那把劍借我。我稍微修理一下。」

「咦？可是這把劍的壽命已經……」

「所以你才會來這座礦山吧？為了搜集新的劍的素材。可是那把劍或許撐不了那麼久。雖然稍微修理了，但請當作我只是稍微延長了它的壽命。」

「可、可以嗎？不過要怎麼做……」

「你看了就知道了。唉，鐵和秘銀的復合素材，要一眼看出來應該很難就是了……那個先放一邊，總之我只是順手多管閑事，你只要當作今天運氣真的很好就好了。我只是一時興起喔？原本應該要收費的。」

「……我知道了，那就拜託你了！」

他接過秘銀之劍後，將它放在魔法陣的中間。藉著輸入魔力展開魔法陣的魔法術式，讓劍浮在空中。周圍浮現出半透明的面板，劍的狀態的詳細資料透過「鑑定」技能出現在腦中。

「唔，比例是鐵45、大馬士革鋼23、秘銀32啊。這還真是……讓人想看看它破損前的樣子呢。」

儘管一邊確認情報，他仍一邊敲著魔法術式的控制鍵盤，針對外觀可以看得出來的龜裂部分建構修復作業，立刻完成了修繕的魔法術式後，小聲說了句：「『練成』。」

他原本是工程師，所以這種工作很快就完成了。在展開的魔法陣內部中，高密度的魔法術式正循環著並發出光芒，打算實行被賦予的命令。乍看之下似乎可以完全修復，但這再怎麼說都是應急處置而已，只能用在單純的作業上。就算只有一次，只要曾經破損過的地方，是不可能完全修復的。

迅速地完成一項工作的他吸了一口菸，吐出煙來。

「完成了。只是這再怎麼說都只能撐到你做出新的劍為止。要是太相信它而繼續使用的話會死的喔？還請千萬要注意。」

「謝、謝謝你。我也沒打算一直使用這把劍，打算要自己準備一把自己的劍。」

「那就沒問題了吧？不過這還是比一般的鐵劍來得像樣啦。暫時應該是不會壞……啊～也要看對手是誰吧～？」

「已經足夠了。這樣我就能去採掘鐵礦了，真的很謝謝你。」

他一邊目送帶著喜悅，勇敢地奔走而去的少年背影，一邊小聲地說著「年輕真好啊～……」一類的話。

將魔法陣收好放回道具欄後，杰羅斯發現不知為何三位女性們都以冷淡的眼神看著他。用一種奇特又帶刺的視線。

「為什麼要用那種彷佛看著垃圾的眼神看我？」

「叔叔……你說過你對小孩子沒興趣吧？」

「手腳意外的快呢……對人家那麼親切……」

「果然是個愛撩妹的傢伙……我也被盯上了……」

完全搞不懂。他只是對少年做了些多管閑事的事情罷了。

他應該沒有做出會被輕蔑至此的行為才對。

「叔叔，你喜歡那種用詞像男性的女生嗎？」

「嘉內也有些不輸給男人的地方，這是你的嗜好嗎？」

「…………（全身顫抖、抖個不停）」

「……What，s？」

接著杰羅斯終於導出結論了。他以為是少年的那孩子，其實是少女的事實。

「那、那是女孩子嗎！？」

「叔叔你沒發現嗎？」

「怎麼可能會有那麼可愛的男孩子……唉，偶爾是會有啦……唔嘿嘿……啊！那怎麼看都是女孩子吧？」

「這個大叔真是差勁透了……比起那個，雷娜……你剛剛那表情……是想起了什麼？」

明明只是做了一點點親切的舉動，卻不知為何被投以更加輕蔑的視線。

真是太沒道理了。

「……等一下。雷娜小姐，你對偶爾會有的美少年做了什麼？」

「什麼啊，就是……欸嘿～嘿嘿嘿嘿嘿嘿♡」

想起了某件事吧，美人的臉扭曲成十分下流的樣子。

作為一個人來說完全不行的遺憾感到了極限，再下去就會令人覺得可怕了。

「將少女誤認為少年的我，和對少年出手的她，哪一個比較差勁呢？」

「「都很差勁！」」

伊莉絲和嘉內的聲音完美的重疊。果然很沒道理。

和雷娜被視為同等級這件事本身也很悲哀。但是比起那個還有更不能接受的事。

「雖然有些無法接受的地方，但我們趕快出發去採礦吧。我一個人去也是不要緊啦。」

「啊，蒙混過去了，叔叔你很差勁耶。」

「真的是，在這裡分開也好吧？在黑暗中也不知道他會做些什麼。」

無視她們擅自說些什麼，杰羅斯朝礦山的入口處前進。

知道關於這個話題自己再多說什麼都沒用。杰羅斯轉換心情，立刻踏入魔物居住的礦山中。雖然背後的雷娜臉上仍帶著下流的笑容搖搖晃晃的走著，但所有人都決定無視這件事。

她似乎還沉浸在回憶之中，就算只是看著她，老實說也很噁心。

平常明明是個正常人，性癖實在是太令人遺憾了。為了不讓人以為是她的伙伴，大家和她保持著一定距離往坑道中前進。為了各自的目的……