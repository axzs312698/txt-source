# novel

- title: ドラゴンに三度轢かれた俺の転生職人ライフ
- title_zh: 三度被龍輾死，我的轉生職人生活
- author: すみもりさい
- illust: 弱電波
- source: http://ncode.syosetu.com/n1212ed/
- cover: https://pbs.twimg.com/media/DU1hAUTVwAAvFI8.jpg
- publisher: syosetu
- date: 2019-03-31T09:08:00+08:00
- status: 連載
- novel_status: 0x0300

## authors

- 澄守 彩

## illusts


## publishers

- syosetu

## series

- name: ドラゴンに三度轢かれた俺の転生職人ライフ

## preface

```
嚮往成為冒險者的我，有一天被龍輾死了。

龍為了表示歉意，讓我轉生。

但在第二次人生中也被龍輾死了。

然後第三次人生也是被龍輾死的。

為了讓我的第四次的人生更加安定。對了... ... 這次以道具強化職人為目標吧。

不知從何時開始就擁有著非凡技能，而且也不知為何會受到各式各樣的美女照料&幫助。
```

## tags

- node-novel
- R15
- syosetu
- いずれ最強主人公
- 強化系職業
- 武装強化で無双
- トラック≒ドラゴン
- 世話焼きドラゴンｘ３
- 幸運EXじゃない？
- 職人スローライフ
- たまに迷宮に潜るよ
- わりとご都合
- 押しかけゆるハーレム

# contribute

- player

# options

## downloadOptions

- noFilePadend: true
- filePrefixMode: 1
- startIndex: 1

## syosetu

- txtdownload_id:
- series_id:
- novel_id: n1212ed

## textlayout

- allow_lf2: true

# link

- [narou.nar.jp](https://narou.nar.jp/search.php?text=n1212ed&novel=all&genre=all&new_genre=all&sort=1&length=0&down=0&up=100) - 小説家になろう　更新情報検索
- [小説情報](https://ncode.syosetu.com/novelview/infotop/ncode/n1212ed/)
- https://masiro.moe/forum.php?mod=viewthread&tid=17924
- 


