「原本是打算教你〈脫水〉的。但是改變主意了」
「為什麼」
「因為我發現你是比想像的還重情義的人阿」
「妳說，我是重情義的人？」
「你大概，學得會〈脫水〉。但是你忍受不住，用〈脫水〉殺人後的那不快的餘味」
「我至今也殺過人。今後也會殺人」
「那麼，為什麼你不在地上戰爭，要潛入迷宮？」
「因為迷宮的戰鬥很有趣」
「你雖然喜歡跟強敵戰鬥，但不喜歡跟弱小的敵人戰鬥吧？」
「什麼？啊啊，對。跟弱小的敵人戰鬥，哪裡有趣了」
「你呢。不是喜歡殺戮。是喜歡戰鬥。在更進一步的話呢。如果是魔獸，就算很弱你也會毫不猶豫地殺掉。但是不會殺弱小的人。不過對你做壊事的人，還有明確地為敵的人就不一樣就是了」
「經妳這麼一說，可能吧」
「用〈脫水〉殺人，根本不是戰鬥。就只是殺掉而已。因為那是防不了的魔法。我是為你好。放棄讓自己學會〈脫水〉吧。記住有這種魔法就好，以防有人對你使用」
「這樣阿。那麼神聖系的〈祝福〉呢？」
「〈祝福〉這魔法，是用神殿透過儀式製造的聖水，來強化人類、防具和武器。雖然就〈祝福〉兩個字，但有各種種類喔」
「強化是怎麼強化？」
「賦予對魔法防禦、賦予對物理防禦，在一定時間內加快攻擊速度、增加劍和槍對魔物的傷害、賦予對妖魔系魔獸的壓倒性傷害之類的。也能奮起同伴的勇氣」

這就很了解。
在原本世界的迷宮，神官戰士和僧侶也會施展這種招式。

「那麼，你應該用不了吧」
「有聖水的話，我也做得到喔」
「為什麼你能碰聖水？」
「那個阿。聖水這詞是人類擅自定下的，本質上，是分了神之力的水。眾神本身，不會判定這世上任何東西的正邪。那是人類一側在做的」
「呼嗯。總之我似乎用不了。等等。你確實說過，覺得〈淨化〉和〈回復〉是同系統的魔法對吧？」
「可能說過吧。是這麼認為的喔」
「但是，那個分類是你做的吧？為什麼〈淨化〉在神聖系裡？」
「那個分類雖然是我整理的，但是把〈淨化〉放進神聖系，是因為這是這世界的常識喔。我自己是認為，〈回復〉和〈淨化〉是同系統，然後分類應該要跟身體系和神聖系都不同。被概括為〈祝福〉的魔法，也分得細一點會比較好」
「說到這就想到了。身體系應該有叫做〈變化〉和〈停止〉的魔法，這又如何」
「你大概沒有適性，就算有，也會很低。〈變身〉是讓自己的一部份肉體變形的魔法，一般是被視為改變臉型的魔法。但是，其實是變形自己的內臟和血管，暫時調整身體機能的魔法。持有上級〈回復〉的你不需要的」
「〈停止〉是讓身體機能下降對吧」
「緊閉在某個地方時使用了〈停止〉的話，就能長時間不吃東西存活。需要某種程度的水就是了。是沒什麼用途的魔法。不過，要研究人體時，〈變身〉和〈停止〉都能幫上很不得了的忙」
「也就是說，〈變身〉和〈停止〉都能對他人施展嗎」
「上級的話能。對戰鬥中的對手施展〈停止〉的話，會只能動得非常緩慢，很有趣喔。不過，能做到這種事的魔法使，會用其他方法打倒敵人吧」
「這樣阿。那麼，〈障壁〉就是最後的魔法了嗎」
「我直接教你的話，對」
「那個一覽表之外，也有很多魔法對吧」
「成山多喔。尤其是光熱系攻擊魔法，有各種變形，同樣的魔法會有不同的名稱，所以只看名稱的話就有幾百種。不過基本的，都在那個表裡」
「沒有〈水刃〉跟〈冰彈〉喔」
「那個抱歉了。〈水刃〉是不小心忘了。〈冰彈〉是以為失傳了。原來復活了阿」
「邱魯的功勞嗎？」
「可能是邱魯的功勞呢」
「邱魯偶爾也會幫上忙呢」
「真的呢。嚇了一跳阿」
「阿，這麼說來，邱魯分類有叫做土系攻擊魔法的。我想想。好像是〈飛礫〉跟〈土樁〉吧」
「〈飛礫〉只是用〈移動〉讓石頭飛出去的魔法而已。〈土樁〉是把土削成樁的樣子，把敵人引誘過去。固定土的方式有訣竅，就算是魔力很少的魔法使，也能做出幾個相當大又堅固的樁。是在騎兵要通過山道時用的就是了。會慢慢來，要花時間，對手運氣好沒上鉤就沒有效果。不適合你吧？」
「不適合我呢」

也就是說，習得〈障壁〉的話，與希拉的師徒關係就結束了。不，希拉是師父這點今後也不會變，但學習魔法的快樂時間已經要結束了。
希拉大概是打算把〈障壁〉的傳授，當作訓練雷肯在戰鬥上能幫上忙的魔法的，最後一課。

那是在委婉地告知，別離之時已經近了。