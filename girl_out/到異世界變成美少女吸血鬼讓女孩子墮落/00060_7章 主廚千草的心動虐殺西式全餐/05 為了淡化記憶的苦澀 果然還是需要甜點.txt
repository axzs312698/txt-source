在城堡地下的昏暗牢獄。
彩路在被鐵窗隔開，只放著陳舊的床和馬桶的房間裡。

「我沒有錯，我沒有錯，我沒有錯，我沒有錯」

憔悴的臉頰，睜著眼睛，從昨晚開始就一覺不睡，重複著同樣的言行。
他無論怎麼看都是瘋了。

儘管如此，彩路還是堅信自己是正確的，完全確信把自己被關在牢裡的国王直屬的騎士們已經被「那些傢伙」洗腦了。

所以，就算是端上來的飯菜，他也絶不食用。
因為他深信那裡面一定混入了人肉。

「我沒有錯，我沒有錯，我沒有錯，我沒有錯」

為什麼要下毒，對於騎士的調查，他甚至連理由都不說。
只是一直在重複著同樣的言詞，已經連溝通都很困難了。

因此，剩下的證據，就只有他使用的毒和作為其容器的小瓶子。
但是，從來沒有聽說過只需要少量攝取就能溶解身體的毒，因為未知毒藥而最先被懷疑的是研究者蕾雅，但她有著完美的不在場證明。

也就是說，還完全搞不懂這是怎麼回事。
恐怕騎士們是無法探知到真相的。
因為他們沒有那麼強的能力，更何況 ── 已經來不及了。

抱著膝蓋，靜坐著的彩路的耳朵裡，聽到了誰走過來的腳步聲。
與運送食物的騎士不同，是多個人的聲音。
儘管如此，對他來說，無論誰來都沒有關係，就像念佛一樣繼續說著同樣的話。
來訪者們在牢房前停下了腳步，向他說道。

「好久不見，風岡先生」

會用那個名字叫他的，只有同樣是被召喚了的誰。
但是，那個聲音既不是櫻奈的，也不是冬花的，甚至都不是都的。
還會有誰哪，彩路微微抬起頭，在視野的邊緣捕捉到了她們的身姿。
櫻奈，冬花，還有⋯⋯不認識的誰。

不，那張臉很面熟，仔細想想，他聽過類似的聲音。

但是，假設她是彩路所認識的她，因為殘疾而導致的臉部肌肉抽筋，聲音更嘶啞，更何況 ── 她不會有這樣充滿自信的表情。
那麼這是誰，眼前這個藐視自己的人到底是誰？

「哦呀，把我都給忘了麼？那可是很無趣的啊」
「日向千草⋯⋯」
「是的，正確答案。太好了，我還以為腦子被玩弄過頭，連記憶都變得曖昧不清了」
「騙人的吧，不是死了麼⋯⋯」
「死了喲，跳樓自殺的同時被召喚了。已經粉身碎骨了。但是多虧了溫柔的某人的關懷才甦醒的，雖然已經不是人類了」
「不是，人類？」

千草以開朗的表情說著，令人震驚的事實。

「是會操縱影子的半吸血鬼。所謂影子，就是指光線照不到的地方，比如說人的大腦就總是在影子中，所以我可以操縱它」
「操縱大腦？難道⋯⋯」
「譬如讓它產生幻聽，幻覺，怎麼，有想法了嗎？」

彩路更加大大地睜著乾涸了的眼球。
頭緒很多。

而且 ──「誰」會對自己做出這種事，關於這個疑問的答案也，對了，如果日向千草還活著的話，她就有著恨他到想殺死他的充分理由。
這時，終於完全抬起了頭的他，第一次認知到了站在牢前的千草，櫻奈，和冬花是怎樣的狀況。

很奇怪，櫻奈和冬花對於關在牢裡的彩路一句話也沒有說。

兩人的脖子上戴著紅色的項圈。
從那裡延伸出來的繩索，在千草的手裡握著。

可是，櫻奈和冬花對於這個，臉上完全沒有一絲討厭的表情。
反而紅著臉頰，濕潤著眼睛，伸出了舌頭，大聲地呼吸著，「嘿嘿嘿」── 簡直就像是被飼養的狗在撒嬌一般陶醉不已。

「是想說櫻奈和冬花怎麼了嗎？」
「怎麼了⋯⋯那個⋯⋯」
「哦，你說的是這項圈啊。正如你所看到的那樣，兩人跪下來舔著我的腳，向我道歉，然後請求，『把我們變成你的所有物』」
「那不可能！」

彩路緊緊抓著鐵柵欄，對著千草怒吼。
雖然很嚇人，但因為在牢房裡，所以缺乏魄力。
她只是「噗哧噗哧」地笑著。

「風岡先生好像還無法接受眼前的現實啊，櫻奈」
「汪」
「哦呀，說起來。記得風岡先生，在幾個月前侵犯我的時候，玩的那個『只能用狗叫聲說話的遊戲』。很懷念吧？忽然想起來了，櫻奈和冬花也試著做一下吧」

千草一邊說明著，一邊撫摸著櫻奈的下巴。

「á Fú，Kú úuu én⋯⋯」

於是櫻奈就如字面意思那樣，發出了狗一樣的撒嬌聲。
主人看到了這一幕，笑了笑，就那樣把臉靠近了她的雙唇，重疊在一起。

「Há Mú，Pú Qiú⋯⋯Qiù，én Fú，Fú úu，Qiú，Qiú Pá⋯⋯！」

千草幾乎一動不動。
只是伸出嘴唇，垂下舌頭。

櫻奈，拚命的吮吸著，不放過任何一滴唾液似的品嘗著。
並且，每次咽下時，身體都震動了一下。

「Há Fú⋯⋯櫻奈，真是好孩子」
「汪⋯⋯」
「汪，汪」

不知是不是因為嫉妒櫻奈，冬花有點生氣的拉著千草的袖子。

「嫉妒了麼，真是拿冬花沒辦法啊，昨晚我都那麼疼愛過你了」
「汪！」
「僅僅那樣還不夠嗎。哼哼，貪得無厭的地方也很可愛哦，和以前的你相比」

千草奪走了冬花的嘴唇，同樣插入了舌頭。
彩路完全無法理解，發生了什麼。

如此討厭千草的櫻奈和冬花，本應和自己一起虐待她的兩人，不知為何竟醉心於那傢伙，甚至還接吻了。
這難道又是幻覺嗎，彩路理所當然的得出了那樣的結論。

可是，這時 ──

「咕！？」

從彩路的喉嚨裡擠出了那個聲音。
劇痛在右臂上奔跑著。

一看，手臂關節朝著不可能的方向彎曲了。

「啊，事先告訴你，現在可沒有在操縱哦，這既不是幻覺也不是幻聽⋯⋯嗯，腳有點累了」
「汪！」

聽到千草的話後，冬花高興地用四肢爬在地上，像是在邀請似的左右搖動屁股。
千草輕輕地撫摸著她的頭，坐在了她的背上。
遲了一步的櫻奈，用不安的表情抬頭看著。
看不下去的主人，當場脫下鞋子和襪子露出了腳給她看 ── 櫻奈仰面躺在千草腳下，吐出了舌頭，「嘿嘿嘿」的，小聲呼吸著。

「我很喜歡懂事的寵物哦」

千草微笑著把腳放在了櫻奈的臉上。

「Mú Fú，én，én Gá⋯⋯Mò，Gó，Qiú⋯⋯Péi Qiá，Réi  Róu⋯⋯Hà，Mú Qiú⋯⋯」

然後，櫻奈開始拚命地舔她的腳。
對於從腳底傳來的令人著急的濕潤的搔癢感，千草的臉頰微微變紅了。

「á én⋯⋯櫻奈和冬花都是好孩子。要給你們兩人獎勵才行」

拚命地用舌頭纏繞腳趾的櫻奈，和因主人的重量而明顯興奮起來的冬花。
千草沉醉了似的愛著，那樣的兩人。
自以為自己「正常」的彩路，根本不能容忍這樣的景象。
所以他大聲喊叫著，在無理面前暴露出憤怒。

「別開玩笑了⋯⋯你們到底是怎麼了！」
「哼哼哼，真是有趣的笑話啊，風岡先生。辱罵毆打腳踢侵犯無抵抗的對手導致其自殺也沒有一句道歉話的你更不正常吧」

彩路被千草不停的言語壓倒了。

「不，但是我能這樣覺醒愛，也正是因為被你們虐待過。正是因為比誰都了解人類的錯誤，所以才能夠徹底放棄人類」
「什麼愛啊，都把峰和凰彌給殺了！」
「殺了他們的是你喲，輕易被妄想所控制，陷入被害妄想，殘酷地殺死了，從心底把你當作朋友來擔心的他們」
「那是因為你操縱了我！」
「我確實故意讓你看到了那些幻覺，但那只是最初而已。至於看到了什麼樣的幻覺，會有什麼樣的結果，這些都是風岡先生你自己所決定的」

為了配合那個，一直讓周圍的吸血鬼們忍耐著。但最後能不能信任峰和凰彌他們，那就是他自己的問題了。

總之，彩路沒有信任友人。
峰從心底以為彩路是自己的好朋友，但彩路只把峰作為「在一起很開心的對手」

如果彩路真心為朋友而著想的話，就應該提議共同抗拒「那些傢伙」，而不是把他們當成敵人。

「總而言之，風岡先生的根性已經腐爛了。所以才把河岸先生和土崎先生殺了。這就是事實」
「咕⋯⋯但是，即便如此，還不是你！」
「不管你再怎麼推脫責任，王国的人們能接受的事實只有一個。你是大量殺人事件的凶手，而且民眾們正們期待著即將開始的處刑」

「居然是⋯⋯處刑」

「啊，還不知道嗎？應該就是在明天，因為即便是讓你活著也無法得到證言，所以通過處刑大量殺人事件的犯人，來消除民眾們對於殺人事件的不安情緒」
「騙人的⋯⋯騙人，騙人，騙人！不是我，我沒有錯！這不是你幹的嗎，千草！可為什麼非要處刑我！對了，現在立刻就把你舉報給騎士們，只要知道真凶是誰，就一定能證明我無罪！」
「哼哼哼，那可真恭喜你了，風岡先生。『我殺人都是因為被吸血鬼給操縱了，所以不是我的錯』，有誰會相信這種證言？」

更何況，現在的彩路是在幻聽和幻覺這種狀態下的。
即使這是事實，他的主張也沒有任何可信性。

「可惡啊啊啊啊啊！千草啊，你這垃圾玩意兒就得意忘形吧，你等著，這次可就不只是侵犯的程度了，殺了你！一邊侵犯一邊殺掉！」
「真難看，怎麼可能做得到那種事。你所能做的，充其量也只有跟籠子裡的動物一樣喊叫著，等著被送上斷頭台，在砍頭的那一刻，為更容易被斷頭台的刀刃所切斷而伸長脖子而已」
「喂，櫻奈，冬花，快清醒過來！這樣真的好嗎，你們被這傢伙利用了！」

大概是意識到了無論對千草說什麼都是沒有用的，彩路呼籲著一直在舔著腳的櫻奈和全身冒著汗水發紅的冬花。

不用說，這是沒有用的。
彩路的話沒有傳達到她們的耳朵裡。

對於已經變成了半吸血鬼的兩人來說，人類的雄性就像在路邊滾動的石子一樣無所謂。
倒不如說，這樣子對話著的千草反而顯得更加溫柔。

「說起來，櫻奈，冬花和風岡先生的關係很好吧。要聽一下評價嗎？關於風岡先生的處刑，你們覺得如何？」

然後，溫柔的千草，代替彩路問了櫻奈和冬花。
回答當然 ──

「汪！」
「汪！」

只有那樣的叫聲。

「哼哼，挺無所謂的樣子啊，風岡先生」
「嗚，嗚⋯⋯嗚嗚嗚嗚嗚嗚嗚嗚！」

連最後的希望都失去了的彩路，一隻手抓著鐵柵欄，崩潰了，叫喚著。

看到了這種情形後，千草感覺到了一種難以言喻的優越感。
啊啊，雖然感嘆到自己居然也有那種低劣的感情，但她並不覺得不好。

雖然這是人類性的一部分，但擺脫自己內心負面記憶的過程也是很有必要的。
風岡彩路對千草來說已經不再是威脅了。

玩弄著她，傷害她的那個男人，現在只能伏倒在千草面前，連呻吟也做不到，真是個可悲的存在。
並且作為他朋友的峰和凰彌也都被彩路親手所殺，櫻奈和冬花最終也互相理解了，直到互相愛著為止。

如果這是可行的 ── 千草確信了所有的計劃都是可以成功的。
既然能與最憎恨的對象心意相通，那麼就等同於能平等的愛著這世界，這世上所有的人類。

來這裡的目的已經實現了。
也就是說，在這一刻，對於千草來說，彩路變成了無價值的存在。
已經，連交談的必要都沒有了。

「那就回去吧，去我們的房間裡」

千草用影子擦去粘在了腳上的櫻奈的唾液，從冬花的椅子上站了起來。
當然，兩人露出了不滿的表情 ──

「一定要給你們兩人獎勵才行啊」

聽到那句話後，馬上又恢復了笑容。
然後站了起來，被千草牽著繩索，以四肢爬行的姿勢離開了監獄。

被留下來的彩路，在那之後，暫時就那麼伏倒在地上。


◇◇◇

第二天，在大量民眾面前執行著，彩路的處刑。

用斷頭台處刑，在這個世界裡，就像是祭典一樣的大活動。
彩路被固定在了台上，站在他旁邊的女性士兵，宣讀了他的罪狀。

在那裡被羅列了內容 ── 關於峰和凰彌，士兵們的殺人事件自不用說，還包含著在城鎮裡發生的各種不可理解的殺人事件。
幾乎沒有離開城堡的彩路是不可能殺人的，但利用了他的異世界人類身份，強行以「用魔法動了手腳」為藉口。

「那些傢伙⋯⋯是那些傢伙幹的，我沒有錯⋯⋯

他甚至在處刑前都還在嘟囔著，當然，誰也沒有理他。

民眾們對於這把城市陷入恐怖深淵的神秘殺人犯的處刑，氣氛上異常的高漲。
他們誰也不知道真相。

除了城堡裡的殺人事件以外，全部都是被冤枉的，真正的犯人是潛伏在城鎮裡的半吸血鬼。
並且，她們已經被侵蝕到了無法挽回的階段了，譬如站立在斷頭台旁的女性士兵們 ── 她們也早已不是人類了。

什麼都不知道。
近期內，就那麼不知不覺間男性死去，女性變為怪物。

『必須要救⋯⋯我。結束了⋯⋯大家，都快要結束了⋯⋯』

他的警告是正確的，本來人類在此時，就應該使用所有的力量去擊潰半吸血鬼。
但是，他的話果然還是傳達不出去。
士兵舉起劍，向支撐著斷頭台刀刃的繩索揮下。

『我 ──』

刀刃無情地斬斷了彩路的脖子。
張著嘴，流著淚的他的頭，就那樣在台上滾動著。
看到了從切口上流出的渾濁血液後，觀眾們沸騰了。


◇◇◇

彩路他們被召喚之後，在原世界 ── 日本，他們的失踪作為大新聞而被報道了。

在教室裡突然消失的六名學生和一名教師。
五名其中的學生品行惡劣，千草受到了殘忍欺凌等等，在媒體和網絡上散佈著各種各樣錯綜複雜的消息。
除了千草的父母以外，親屬們都在拚命地尋找著他們的行踪，呼籲著哪怕只有一點目擊情報也好，請告知我們。

誰也不知道。
男性學生們已經死去了。
誰也不知道。
女性學生們已經不是人類了。

是的，誰也不知道。
既然是消失在了異世界，那就再無法知曉了。
事件不久後就會被漸漸淡忘，經過了七年而被宣告為失踪的他們，會被當成死人來對待。
也沒有家人們的看護。
屍體就像垃圾一樣被丟棄。
靈魂彷徨在陌生的世界裡。

『這很適合大家哦』

千草仰望著虛空，小聲說道。
她的臉上浮現出了苦悶的表情，仿彿看到了三人的靈魂一般 ── 她微笑了，幸福地微笑了。